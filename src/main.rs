extern crate reqwest;
extern crate tiny_http;

mod lib;

use core::time::Duration;
use lib::ThreadPool;
use reqwest::blocking::Client;
use reqwest::{header::HeaderName, Method, Url};
use std::fmt::Display;
use std::net::SocketAddr;
use std::str::FromStr;
use std::sync::Arc;
use std::{error, fmt};
use structopt::StructOpt;

#[derive(Clone)]
struct Mapping {
    /// The domain matched by this mapping.
    realm: String,
    /// The port requests to this domain will be forwarded to.
    port: i32,
}

#[derive(Debug)]
struct MappingParseError(String);

impl error::Error for MappingParseError {}

impl Display for MappingParseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Could not parse \"{}\" as a domain:port mapping.", &self)
    }
}

impl FromStr for Mapping {
    type Err = MappingParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some((realm, port_str)) = s.rsplit_once(':') {
            let port = port_str
                .parse::<i32>()
                .expect("Port number should be a number");
            return Ok(Mapping {
                realm: realm.to_string(),
                port: port,
            });
        } else {
            return Err(MappingParseError(s.to_string()));
        }
    }
}

#[derive(StructOpt)]
#[structopt(
    name = "http-router",
    about = "Routes HTTP requests using the Host header to a port.",
    after_help = concat!(
        "EXAMPLES:\n",
        "   http-router -p 5000 foo.rec.la:4500 bar.rec.la:4501",
    ),
)]
struct Cli {
    /// The port to listen in.
    #[structopt(short = "p", long = "port")]
    port: i32,
    /// 'host:port' mappings.
    mappings: Vec<Mapping>,
    /// Timeout in seconds when waiting upstream response.
    #[structopt(short = "t", long = "timeout", default_value = "1")]
    timeout: u64,
}

fn main() {
    let args = Cli::from_args();
    let mappings = Arc::new(args.mappings);

    println!("Listening in port {}", args.port);

    for mapping in mappings.as_ref() {
        println!("\t{} -> {}:{}", mapping.realm, "localhost", mapping.port);
    }

    let addrs = [
        SocketAddr::from_str(format!("[::1]:{}", args.port).as_str()).unwrap(),
        SocketAddr::from_str(format!("127.0.0.1:{}", args.port).as_str()).unwrap(),
    ];
    let server = tiny_http::Server::http(&addrs[..]).expect("Could not create HTTP server.");
    let pool = ThreadPool::new(4);
    let request_timeout = args.timeout;

    loop {
        let mappings = mappings.clone();
        match server.recv() {
            Ok(request) => {
                pool.execute(move || handle_connection(request, mappings, request_timeout))
            }
            Err(e) => println!("Error receiving request: {}", e),
        }
    }
}

fn handle_connection(mut request: tiny_http::Request, mappings: Arc<Vec<Mapping>>, timeout: u64) {
    if let Some(mapping) = mapping_from_headers(&request.headers(), &mappings) {
        println!(
            "Forwarding request for {} to port {}: {}",
            mapping.realm,
            mapping.port,
            request.url()
        );
        let client = Client::new();
        let method = upstream_request_method(&request);
        let upstream_request = client
            .request(method, upstream_request_url(&mapping, &request))
            .headers(to_headermap(request.headers()))
            .body(to_reqwest_body(&mut request))
            .timeout(Duration::from_secs(timeout))
            .build()
            .expect("Could not build upstream request.");
        match client.execute(upstream_request) {
            Ok(r) => {
                let status_code = tiny_http::StatusCode::from(r.status().as_u16());
                let content_length = r.content_length().map(|v| v as usize);
                let response = tiny_http::Response::new(
                    status_code,
                    from_headermap(r.headers()),
                    r,
                    content_length,
                    None,
                );
                let _ = request.respond(response);
            }
            Err(e) => {
                println!("Could not complete upstream request: {:}", e);
                let _ = request.respond(tiny_http::Response::empty(502));
            }
        }
    } else {
        let _ = request.respond(tiny_http::Response::empty(404));
    }
}

fn to_headermap(headers: &[tiny_http::Header]) -> reqwest::header::HeaderMap {
    let mut r = reqwest::header::HeaderMap::new();
    for header in headers {
        r.insert(
            HeaderName::from_str(header.field.as_str().as_str()).unwrap(),
            header.value.as_str().parse().unwrap(),
        );
    }
    r
}

fn from_headermap(headers: &reqwest::header::HeaderMap) -> Vec<tiny_http::Header> {
    let mut r = Vec::with_capacity(headers.iter().count());
    for (key, value) in headers.iter() {
        r.push(tiny_http::Header::from_bytes(key.as_str(), value.as_ref()).unwrap());
    }
    r
}

fn to_reqwest_body(request: &mut tiny_http::Request) -> reqwest::blocking::Body {
    let mut body = Vec::new();
    // FIXME: This reads all the request body into RAM.
    let _ = request
        .as_reader()
        .read_to_end(&mut body)
        .expect("Could not download request body.");
    reqwest::blocking::Body::from(body)
}

fn mapping_from_headers(headers: &[tiny_http::Header], mappings: &Vec<Mapping>) -> Option<Mapping> {
    for header in headers {
        if header.field.equiv("Host") {
            let host = header
                .value
                .as_str()
                .rsplit_once(':')
                .map(|(host, _)| host)
                .unwrap_or(header.value.as_str());
            for mapping in mappings {
                if mapping.realm.eq_ignore_ascii_case(host) {
                    return Some(mapping.clone());
                }
            }
        }
    }
    None
}

fn upstream_request_url(mapping: &Mapping, request: &tiny_http::Request) -> Url {
    let url_str = format!("http://localhost:{}{}", mapping.port, request.url());
    Url::parse(url_str.as_str()).expect("Could not build upstream request URL.")
}

fn upstream_request_method(request: &tiny_http::Request) -> Method {
    Method::from_str(request.method().as_str()).expect("Could not build upstream request method.")
}
