# http-router

Simple HTTP reverse proxy.

## Usage

```console
$ http-router --help
http-router 0.1.0
Routes HTTP requests using the Host header to a port.

USAGE:
    http-router --port <port> [mappings]...

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -p, --port <port>    The port to listen in

ARGS:
    <mappings>...    'host:port' mappings

EXAMPLES:
   http-router -p 5000 foo.rec.la:4500 bar.rec.la:4501
```

```console
$ http-router -p 5000 a.example.com:3000 b.example.com:3001
Listening in port 5000
        foo.rec.la -> localhost:4500
        bar.rec.la -> localhost:4501
```
